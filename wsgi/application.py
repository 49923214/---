#coding: utf-8
'''
當 CherryPy 程式尚未牽涉 'OPENSHIFT_DATA_DIR' 資料存取時, 近端與遠端程式完全相同.
但若牽涉必須透過網際介面進行 persistent 資料存取時, 則必須區分近端 data 目錄與遠端目錄.
本程式執行需要 brython.py 與 menu.py
'''
import cherrypy
import os
# 將同目錄下的 brython.py 導入
import sys
# 確定程式檔案所在目錄
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
# 將目前檔案所在目錄納入 sys 模組搜尋目錄中
sys.path.append(_curdir)
import brython
# 將同目錄下的 menu.py 導入
import menu

# 以下為產生 html 標註的特定函式
def htmlTitle(title):
    return '''
<!DOCTYPE html>
<html>
<head>
<! charset meta 設定應該在 title 標註之前, 以免因產生編碼錯誤的 title -->
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>'''+title+"</title>"

def htmlCSS(css):
    return '''
<link rel="stylesheet" type="text/css" href="'''+css+'''">
</head>
'''

class Brython(object):

    @cherrypy.expose
    def about(self):
        return htmlTitle("有關本網站")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
本網站為電腦輔助設計實習課程分組網站<br />
採用 CherryPy 與 Python 建置<br />
</body>
</html>
'''

    @cherrypy.expose
    def index(self):
        return htmlTitle("Brython and Pulldown menu")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
'''+brython.BrythonConsole()+ \
'''
</body>
</html>
'''

    @cherrypy.expose
    def creoParts(self):
        return htmlTitle("Creo 零件")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
展示 Creo 的零件檔案<br/>
選定基準面 繪製草圖後長出<br/><a href='static/images/49923214-02.prt.1'>第二題</a><br/>


<img src="/static/images/CREO3.jpg"height="400" width="600"></img><br />
<br/>
後在一平面上做半圓形草圖 <br/>
<img src="/static/images/CREO2.jpg"height="400" width="600"></img><br />
<br/>
長出後同第一步驟繪製<br/>
<img src="/static/images/CREO1.jpg"height="400" width="600"></img><br />
<br/>
長出 (需要選擇方向長出)<br/>
<img src="/static/images/CREO0.jpg"height="400" width="600"></img><br />

<object type="application/x-shockwave-flash" data="/static/images/player_flv_maxi.swf" width="600" height="400">
    <param name="movie" value="/static/images/player_flv_maxi.swf" />
    <param name="allowFullScreen" value="true" />
    <param name="FlashVars" value="flv=/static/images/49923214_02_Creo.flv&amp;width=600&amp;height=400&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;startimage=/static/images/49923214_02_Creo.flv&amp;showfullscreen=1&amp;bgcolor1=189ca8&amp;bgcolor2=085c68&amp;playercolor=085c68" />
</object>
<br />
</body>
</html>
'''




    @cherrypy.expose
    def creoParts(self):
        return htmlTitle("零件繪圖")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
展示 Creo 的零件檔案<br/>
選定基準面 繪製草圖後長出<br/><a href='static/images/49923214-02.prt.1'>第二題</a><br/>


<img src="/static/images/CREO3.jpg"height="400" width="600"></img><br />
<br/>
後在一平面上做半圓形草圖 <br/>
<img src="/static/images/CREO2.jpg"height="400" width="600"></img><br />
<br/>
長出後同第一步驟繪製<br/>
<img src="/static/images/CREO1.jpg"height="400" width="600"></img><br />
<br/>
長出 (需要選擇方向長出)<br/>
<img src="/static/images/CREO0.jpg"height="400" width="600"></img><br />

<object type="application/x-shockwave-flash" data="/static/images/player_flv_maxi.swf" width="600" height="400">
    <param name="movie" value="/static/images/player_flv_maxi.swf" />
    <param name="allowFullScreen" value="true" />
    <param name="FlashVars" value="flv=/static/images/49923214_02_Creo.flv&amp;width=600&amp;height=400&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;startimage=/static/images/49923214_02_Creo.flv&amp;showfullscreen=1&amp;bgcolor1=189ca8&amp;bgcolor2=085c68&amp;playercolor=085c68" />
</object>
<br />
</body>
</html>
'''

    @cherrypy.expose
    def creoasm(self):
        return htmlTitle("Creo組立")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
Creo 組立檔案<br/>
選定基準面<br/>
<img src="/static/images/Nut8.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut7.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut6.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut5.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut4.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut3.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut2.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut1.jpg"height="400" width="600"></img><br />
<br/>
組合固定<br/>
<img src="/static/images/Nut0.jpg"height="400" width="600"></img><br />



<object type="application/x-shockwave-flash" data="/static/images/player_flv_maxi.swf" width="600" height="400">
    <param name="movie" value="/static/images/player_flv_maxi.swf" />
    <param name="allowFullScreen" value="true" />
    <param name="FlashVars" value="flv=/static/images/49923214_02_Creo.flv&amp;width=600&amp;height=400&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;startimage=/static/images/49923214_02_Creo.flv&amp;showfullscreen=1&amp;bgcolor1=189ca8&amp;bgcolor2=085c68&amp;playercolor=085c68" />
</object>
<br />
</body>
</html>
'''
    @cherrypy.expose
    def solvespace(self):
        return htmlTitle("SolveSpace 零件")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
Solvespace 零件檔案<br/>
選一基準面做草圖<br/>
<img src="/static/images/sovespace01.jpg"height="400" width="600"></img><br />
<br/>
長出<br/>
<img src="/static/images/sovespace02.jpg"height="400" width="600"></img><br />
<br/>
再一面上做圓形草圖<br/>
<img src="/static/images/sovespace03.jpg"height="400" width="600"></img><br />
<br/>
鑽通孔<br/>
<img src="/static/images/sovespace04.jpg"height="400" width="600"></img><br />
<br/>
再繪製一半圓面<br/>
<img src="/static/images/sovespace05.jpg"height="400" width="600"></img><br />
<br/>
長出<br/>
<img src="/static/images/sovespace06.jpg"height="400" width="600"></img><br />
<br/>
在圓柱上畫圓<br/>
<img src="/static/images/sovespace07.jpg"height="400" width="600"></img><br />
<br/>
鑽孔<br/>
<img src="/static/images/sovespace08.jpg"height="400" width="600"></img><br />
<br/>
<br />
</body>
</html>
'''

    @cherrypy.expose
    def introMember1(self):
        return htmlTitle("組員 1 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 李季霖<br />
組員學號: 49923214<br />
組員相片:<img src="/static/images/member1.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember2(self):
        return htmlTitle("組員 2 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 吳柏毅<br />
組員學號: 49923210<br />
組員相片:<img src="/static/images/member2.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember3(self):
        return htmlTitle("組員 3 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 盧毅民<br />
組員學號: 40123157<br />
組員相片:<img src="/static/images/member3.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember4(self):
        return htmlTitle("組員 4 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 藍逸閔<br />
組員學號: 49923248<br />
組員相片:<img src="/static/images/member4.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember5(self):
        return htmlTitle("組員 5 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 鄭寶麟<br />
組員學號: 49923244<br />
組員相片:<img src="/static/images/member5.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''
    @cherrypy.expose
    def introMember6(self):
        return htmlTitle("組員 6 介紹")+ \
                    htmlCSS("/static/jscript/style.css")+ \
'''
<body>
<div class="container">
'''+menu.GenerateMenu()+ \
'''
</div>
<!-- 以上為表單區域 -->
組員姓名: 蔣秉軒<br />
組員學號: 49923241<br />
組員相片:<img src="/static/images/member6.jpg"></img><br />
組員專長:<br />
組員介紹:<br />

</body>
</html>
'''

# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/Brython1.2-20131109-201900':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/Brython1.2-20131109-201900"},
        '/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }

# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(Brython(), config = application_conf)
else:
    # 近端執行啟動
    cherrypy.quickstart(Brython(), config = application_conf)
